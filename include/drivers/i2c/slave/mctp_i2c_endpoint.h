
#ifndef ZEPHYR_INCLUDE_DRIVERS_MCTP_I2C_ENDPOINT_H_
#define ZEPHYR_INCLUDE_DRIVERS_MCTP_I2C_ENDPOINT_H_

int mctp_i2c_endpoint_write(const struct device *dev, const uint8_t *buf,
			    uint32_t num_bytes, uint16_t addr);

int mctp_i2c_endpoint_read(const struct device *dev, uint8_t *buf, uint32_t buf_size);
uint8_t mctp_i2c_endpoint_addr(const struct device *dev);

#endif /* ZEPHYR_INCLUDE_DRIVERS_MCTP_I2C_ENDPOINT_H_ */