/*
 * Copyright (c) 2021 ARM ltd
 *
 * SPDX-License-Identifier: Apache-2.0
 */

#define DT_DRV_COMPAT generic_mctp_i2c_endpoint

#include <sys/util.h>
#include <kernel.h>
#include <errno.h>
#include <drivers/i2c.h>
#include <string.h>
#include <drivers/i2c/slave/mctp_i2c_endpoint.h>

#define LOG_LEVEL CONFIG_I2C_LOG_LEVEL
#include <logging/log.h>
LOG_MODULE_REGISTER(i2c_slave);

#define ENDPOINT_BUF_SIZE 256

struct mctp_i2c_endpoint_data {
	const struct device *i2c_controller;
	struct i2c_slave_config config;
	uint8_t *buffer;
	uint32_t buffer_idx;
	struct k_sem *sem_rx_rdy;
};

struct mctp_i2c_endpoint_config {
	char *controller_dev_name;
	uint8_t address;
	uint8_t *buffer; //todo: dual buffer
	struct k_sem *sem_rx_rdy;
};

int mctp_i2c_endpoint_write(const struct device *dev, const uint8_t *buf,
			    uint32_t num_bytes, uint16_t addr)
{
	struct mctp_i2c_endpoint_data *data = dev->data;
	//i2c_write() takes 7 bits addr
	printk("%s addr=0x%02x len=%d\n", __func__, addr>>1, num_bytes);
	return i2c_write(data->i2c_controller, buf, num_bytes, addr>>1); 
}

int mctp_i2c_endpoint_read(const struct device *dev, uint8_t *buf, uint32_t buf_size)
{
	struct mctp_i2c_endpoint_data *data = dev->data;
	const struct mctp_i2c_endpoint_config *config = dev->config;

	k_sem_take(data->sem_rx_rdy, K_FOREVER);	

	if (!buf || (buf_size < data->buffer_idx)) {
		return -EINVAL;
	}
	
	// set first byte destination to myself
	buf[0] = config->address<<1; //shitf left bit 1 for r/w bit
	memcpy(buf+1, data->buffer, data->buffer_idx);
	return data->buffer_idx+1;
}

uint8_t mctp_i2c_endpoint_addr(const struct device *dev)
{
	const struct mctp_i2c_endpoint_config *config = dev->config;
	return config->address;
}

static int mctp_i2c_endpoint_write_requested(struct i2c_slave_config *config)
{
	struct mctp_i2c_endpoint_data *data = CONTAINER_OF(config,
						struct mctp_i2c_endpoint_data,
						config);

	data->buffer_idx = 0;
	return 0;
}

static int mctp_i2c_endpoint_write_received(struct i2c_slave_config *config,
				       uint8_t val)
{
	struct mctp_i2c_endpoint_data *data = CONTAINER_OF(config,
						struct mctp_i2c_endpoint_data,
						config);

	data->buffer[data->buffer_idx++] = val;
	if(data->buffer_idx >= ENDPOINT_BUF_SIZE) {
		data->buffer_idx--; // overwrite last byte if buffer overflow
	}

	return 0;
}

static int mctp_i2c_endpoint_read_requested(struct i2c_slave_config *config,
				       uint8_t *val)
{
	return -ENOTSUP;
}

static int mctp_i2c_endpoint_read_processed(struct i2c_slave_config *config,
				       uint8_t *val)
{
	return -ENOTSUP;
}

static int mctp_i2c_endpoint_stop(struct i2c_slave_config *config)
{
	struct mctp_i2c_endpoint_data *data = CONTAINER_OF(config,
						struct mctp_i2c_endpoint_data,
						config);

	//signal i2c_receiver_read api
	k_sem_give(data->sem_rx_rdy);
	return 0;
}

static int mctp_i2c_endpoint_register(const struct device *dev)
{
	struct mctp_i2c_endpoint_data *data = dev->data;

	return i2c_slave_register(data->i2c_controller, &data->config);
}

static int mctp_i2c_endpoint_unregister(const struct device *dev)
{
	struct mctp_i2c_endpoint_data *data = dev->data;

	return i2c_slave_unregister(data->i2c_controller, &data->config);
}

static const struct i2c_slave_driver_api api_funcs = {
	.driver_register = mctp_i2c_endpoint_register,
	.driver_unregister = mctp_i2c_endpoint_unregister,
};

static const struct i2c_slave_callbacks mctp_i2c_endpoint_callbacks = {
	.write_requested = mctp_i2c_endpoint_write_requested,
	.read_requested = mctp_i2c_endpoint_read_requested,
	.write_received = mctp_i2c_endpoint_write_received,
	.read_processed = mctp_i2c_endpoint_read_processed,
	.stop = mctp_i2c_endpoint_stop,
};

static int mctp_i2c_endpoint_init(const struct device *dev)
{
	struct mctp_i2c_endpoint_data *data = dev->data;
	const struct mctp_i2c_endpoint_config *cfg = dev->config;

	data->i2c_controller =
		device_get_binding(cfg->controller_dev_name);
	if (!data->i2c_controller) {
		LOG_ERR("i2c controller not found: %s",
			    cfg->controller_dev_name);
		return -EINVAL;
	}

	data->buffer = cfg->buffer;
	data->config.address = cfg->address;
	data->config.callbacks = &mctp_i2c_endpoint_callbacks;
	data->sem_rx_rdy = cfg->sem_rx_rdy;

	return 0;
}

#define MCTP_I2C_ENDPOINT_INIT(inst)						\
	static struct mctp_i2c_endpoint_data				\
		mctp_i2c_endpoint_##inst##_dev_data;			\
									\
	static uint8_t							\
	mctp_i2c_endpoint_##inst##_buffer[ENDPOINT_BUF_SIZE];	\
	static K_SEM_DEFINE(mctp_i2c_endpoint_##inst##_sem_rx_rdy, 0, 1);	\
									\
	static const struct mctp_i2c_endpoint_config			\
		mctp_i2c_endpoint_##inst##_cfg = {			\
		.controller_dev_name = DT_INST_BUS_LABEL(inst),		\
		.address = DT_INST_REG_ADDR(inst),			\
		.buffer = mctp_i2c_endpoint_##inst##_buffer,		\
		.sem_rx_rdy = &mctp_i2c_endpoint_##inst##_sem_rx_rdy \
	};								\
									\
	DEVICE_DT_INST_DEFINE(inst,					\
			    &mctp_i2c_endpoint_init,			\
			    NULL,			\
			    &mctp_i2c_endpoint_##inst##_dev_data,	\
			    &mctp_i2c_endpoint_##inst##_cfg,		\
			    POST_KERNEL,				\
			    CONFIG_I2C_SLAVE_INIT_PRIORITY,		\
			    &api_funcs);

DT_INST_FOREACH_STATUS_OKAY(MCTP_I2C_ENDPOINT_INIT)
